
<a name="v0.4.0-pre"></a>
## [v0.4.0-pre](https://gitlab.com/uag/loadouts-library/compare/v0.3.0-b190105170330...v0.4.0-pre) (2019-03-26)




<a name="v0.3.0-pre"></a>
## [v0.3.0-pre](https://gitlab.com/uag/loadouts-library/compare/v0.3.1-r2...v0.3.0-pre) (2019-01-05)


<a name="v0.3.1-r2"></a>
## [v0.3.1-r2](https://gitlab.com/uag/loadouts-library/compare/v0.2.5-b190105155428...v0.3.1-r2) (2019-01-05)




<a name="v0.2.5-pre"></a>
## [v0.2.5-pre](https://gitlab.com/uag/loadouts-library/compare/v0.2.3-b190103200656...v0.2.5-pre) (2019-01-05)




<a name="v0.2.3-pre"></a>
## [v0.2.3-pre](https://gitlab.com/uag/loadouts-library/compare/v0.2.0-b190103172646...v0.2.3-pre) (2019-01-03)




<a name="v0.2.0-pre"></a>
## [v0.2.0-pre](https://gitlab.com/uag/loadouts-library/compare/v0.1.5-b190103163204...v0.2.0-pre) (2019-01-03)




<a name="v0.1.5-pre"></a>
## [v0.1.5-pre](https://gitlab.com/uag/loadouts-library/compare/v0.1.2-b190103161841...v0.1.5-pre) (2019-01-03)




<a name="v0.1.2-pre"></a>
## [v0.1.2-pre](https://gitlab.com/uag/loadouts-library/compare/v0.1.0-b190103152959...v0.1.2-pre) (2019-01-03)




<a name="v0.1.0-pre"></a>
## v0.1.0-pre (2019-01-03)

